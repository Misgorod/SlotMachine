﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Slot
{
    [CreateAssetMenu(fileName = "Slots", menuName = "Slot/Slots")]
    public class SlotScriptable : ScriptableObjectInstaller<SlotScriptable>
    {
        public List<Slot.Settings> slotSettings;

        public override void InstallBindings()
        {
            Container.Bind<List<Slot.Settings>>().FromInstance(slotSettings);
        }
    }
}
