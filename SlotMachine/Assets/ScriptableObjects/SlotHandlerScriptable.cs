﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zenject;
using UnityEngine;

namespace Slot
{
    [CreateAssetMenu(fileName = "Slots", menuName = "Slot/SlotsHandler")]
    public class SlotHandlerScriptable : ScriptableObjectInstaller<SlotHandlerScriptable>
    {
        public float rollSpeed;
        public float rollTime;

        public override void InstallBindings()
        {
            Container.BindInstance(rollSpeed).WithId("Speed");
            Container.BindInstance(rollTime).WithId("Time");
        }
    }
}
