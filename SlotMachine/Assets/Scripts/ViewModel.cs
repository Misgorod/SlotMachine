﻿using System;
using Object = System.Object;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using TMPro;

namespace Slot
{
    public class ViewModel : MonoBehaviour
    {
        [SerializeField]
        private Button rollButton;
        [SerializeField]
        private Button multiplyButton;
        [SerializeField]
        private TextMeshProUGUI moneyText;
        [SerializeField]
        private TextMeshProUGUI rollCostText;
        [SerializeField]
        private TextMeshProUGUI winAmountText;
        [SerializeField]
        private List<SlotsHandler> slotsHandlers;

        private List<List<Pair<Vector2, Slot>>> positions = new List<List<Pair<Vector2, Slot>>>();

        private float rollTime;
        
        private Player player;
        private SignalBus signalBus;

        [Inject]
        public void Construct(SignalBus signalBus, Player player, [Inject(Id = "Time")] float rollTime)
        {
            this.signalBus = signalBus;
            this.player = player;
            this.rollTime = rollTime;
            
        }

        private void Awake()
        {
            player.OnUpdateData += UpdatePlayerData;
        }

        private void Start()
        {
            rollButton.OnClick += Roll;
            multiplyButton.OnClick += Multiply;

            foreach (var handler in slotsHandlers)
            {
                positions.Add(handler.Positions);
                handler.OnRollComplete += CheckWin;
            }
        }

        private int completedHandlers = 0;

        public void CheckWin(Object s, EventArgs e)
        {
            completedHandlers++;
            if (completedHandlers == 5)
            {
                completedHandlers = 0;
                player.CheckWin(positions);
            }
        }

        public void Roll(Object s, EventArgs e)
        {
            if (player.CanRoll())
            {
                foreach (var slotsHandler in slotsHandlers)
                {
                    slotsHandler.Roll();
                }
                player.Roll();
            }

            rollButton.Deactivate(rollTime);
        }

        public void Multiply(Object s, EventArgs e)
        {
            player.Multiply();
        }

        public void UpdatePlayerData(Object s, UpdateDataEventArgs e)
        {
            this.moneyText.text = e.Money.ToString("G0") + " $";
            this.rollCostText.text = e.RollCost.ToString("G0") + " $";
            this.winAmountText.text = e.WinAmount.ToString("G0") + " $";
        }

    }
}
