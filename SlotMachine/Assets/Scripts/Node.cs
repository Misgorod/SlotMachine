﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Slot
{
    public class Node
    {
        public List<Node> Nodes { set; get; }

        public Slot Slot { get; set; }

        public Node (Slot slot) : this(new List<Node>(), slot) { }
        
        public Node(List<Node> nodes, Slot slot)
        {
            Nodes = nodes;
            Slot = slot;
        }

    }
}
