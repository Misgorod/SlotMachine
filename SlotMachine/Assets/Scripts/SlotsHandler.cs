﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using Zenject;


namespace Slot
{
    public class SlotsHandler : MonoBehaviour
    {
        public List<Pair<Vector2, Slot>> Positions { get { return positions; } }
        public event EventHandler OnRollComplete;

        [SerializeField]
        private List<Slot> slots;
        [SerializeField]
        private float spacing = 5;

        private SignalBus signalBus;

        private float rollSpeed;
        private float rollTime;

        private float currentTime = 0;
        private bool isRolling = false;

        private Vector2 startPosition;
        private Vector2 finishPosition;

        private List<Pair<Vector2, Slot>> positions = new List<Pair<Vector2, Slot>>();

        [Inject]
        public void Construct([Inject(Id = "Speed")] float rollSpeed, [Inject(Id = "Time")] float rollTime)
        {
            this.rollSpeed = rollSpeed;
            this.rollTime = rollTime;
        }

        void Start()
        {
            Debug.Assert(slots.Count == 7);
            startPosition = slots[0].rectTransform.anchoredPosition;
            finishPosition = slots[slots.Count - 1].rectTransform.anchoredPosition - new Vector2(0, slots[slots.Count - 1].rectTransform.rect.height + spacing);

            foreach (Slot slot in slots)
            {
                positions.Add(new Pair<Vector2, Slot>(slot.rectTransform.anchoredPosition, slot));
            }
            positions.Add(new Pair<Vector2, Slot>(finishPosition, null));

            Debug.Assert(positions.Count - slots.Count == 1);

            int i = 1;
            foreach (Slot slot in slots)
            {
                slot.Set(positions, i, rollSpeed);
                i++;
            }
        }

        public void Roll()
        {
            isRolling = true;
            currentTime = 0;
        }

        void Update()
        {
            if (currentTime >= rollTime && isRolling && slots.All((x) => x.IsStop == true))
            {
                isRolling = false;
                OnRollComplete(this, EventArgs.Empty);
            }

            if (slots.All((x) => x.IsStop == true) && isRolling)
            {
                slots.ForEach(x => x.Move());
            }

            currentTime += Time.deltaTime;

            
        }
    }
}
