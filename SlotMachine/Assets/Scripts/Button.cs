﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Slot
{
    public class Button : MonoBehaviour, IPointerClickHandler
    {
        public virtual event EventHandler<EventArgs> OnClick;

        public bool IsActive { get; private set; }

        private float currentTime;
        private float waitTime;

        private void Start()
        {
            IsActive = true;
        }

        public virtual void OnPointerClick(PointerEventData data)
        {
            if (IsActive)
            {
                OnClick(this, EventArgs.Empty);
            }
        }

        public void Deactivate(float time)
        {
            IsActive = false;
            waitTime = time;
            currentTime = 0;
        }

        private void Update()
        {
            currentTime += Time.deltaTime;
            if (currentTime >= waitTime)
            {
                IsActive = true;
            }
        }
    }
}
