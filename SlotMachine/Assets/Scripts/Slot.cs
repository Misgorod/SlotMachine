﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using Zenject;

namespace Slot
{
    public enum SlotType
    {
        Slot0,
        Slot1,
        Slot2,
        Slot3,
        Slot4,
        Slot5,
        Slot6
    }

    public class Slot : MonoBehaviour
    {
        public RectTransform rectTransform { get; private set; }
        public bool IsStop { get; private set; }
        public SlotType Type { get { return settings.type; } }

        private Image image;
        private float speed = 0;
        private List<Pair<Vector2, Slot>> positions = new List<Pair<Vector2, Slot>>();
        private int currentDestinationIndex = 0;

        private Settings settings;
        private List<Settings> settingsList;

        [SerializeField]
        private bool debug;

        [Inject]
        public void Construct(List<Settings> settings)
        {
            settingsList = settings;
        }

        private bool Equal(Vector2 first, Vector2 Second)
        {
            if (Mathf.Abs(first.x - Second.x) < 0.1 && Mathf.Abs(first.y - Second.y) < 0.1)
            {
                return true;
            }
            return false;
        }

        public void SetSettings(Settings settings)
        {
            this.settings = settings;
            image.overrideSprite = settings.sprite;

        }

        public void SetRandomSettings()
        {
            Random.InitState(System.DateTime.Now.Millisecond + this.GetHashCode());
            Settings localSettings;
            do
            {
                localSettings = settingsList[Random.Range(0, settingsList.Count)];
            }
            while (localSettings.type == this.settings?.type);
            SetSettings(localSettings);
        }

        public void Set(List<Pair<Vector2, Slot>> positions, int currentDestinationIndex, float speed)
        {
            this.positions = positions;
            this.currentDestinationIndex = currentDestinationIndex;
            this.speed = speed;
            IsStop = true;
        }

        private void Awake()
        {
            image = GetComponent<Image>();
            rectTransform = GetComponent<RectTransform>();
        }

        private void Start()
        {
            Random.InitState(System.DateTime.Now.Millisecond + this.GetHashCode());
            SetRandomSettings();
            
        }

        public void Move()
        {
            IsStop = false;
            currentDestinationIndex++;
            if (currentDestinationIndex > positions.Count - 1)
            {
                

                currentDestinationIndex = 1;
                rectTransform.anchoredPosition = positions[0].First;
                StartCoroutine(MoveCorChange());
            }
            else
            {
                StartCoroutine(MoveCor());
            }
            positions[currentDestinationIndex].Second = this;

            
        }

        private IEnumerator MoveCor()
        {
            float t = 0;
            while (!Equal(rectTransform.anchoredPosition, positions[currentDestinationIndex].First))
            {
                rectTransform.anchoredPosition = Vector2.Lerp(rectTransform.anchoredPosition, positions[currentDestinationIndex].First, t);
                if (t < 1)
                {
                    t += speed * Time.deltaTime;
                }
                else
                {
                    t = 1;
                }
                yield return new WaitForEndOfFrame();
            }
            IsStop = true;
        }

        private IEnumerator MoveCorChange()
        {
            float t = 0;
            while (!Equal(rectTransform.anchoredPosition, positions[currentDestinationIndex].First))
            {
                rectTransform.anchoredPosition = Vector2.Lerp(rectTransform.anchoredPosition, positions[currentDestinationIndex].First, t);
                if (t < 1)
                {
                    t += speed * Time.deltaTime;
                }
                else
                {
                    t = 1;
                }
                yield return new WaitForEndOfFrame();
            }
            IsStop = true;
            //SetRandomSettings();
        }

        [System.Serializable]
        public class Settings
        {
            public Sprite sprite;
            public SlotType type;
        }

    }
}
