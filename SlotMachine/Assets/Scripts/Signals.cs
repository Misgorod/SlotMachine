﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Slot
{
    public class RollStartsSignal { }

    public class RollSignal { }

    public class UpdatePlayerDataSignal
    {
        public decimal Money { get; set; }
        public decimal RollCost { get; set; }

        public UpdatePlayerDataSignal(decimal money, decimal rollCost)
        {
            Money = money;
            RollCost = rollCost;
        }
    }

    public class MultiplySignal
    {
        public decimal Multiplier { get; set; }

        public MultiplySignal(decimal multiplier)
        {
            Multiplier = multiplier;
        }
    }
}
