﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Slot
{
    public class WinHandler
    {
        private List<Node> graph = new List<Node>();

        private List<List<Node>> pathLists = new List<List<Node>>();

        /* Graph has this layout:
         * 
         * 14   13  12
         * 11   10  9
         * 8    7   6
         * 5    4   3
         * 2    1   0
         * 
         * where number is index in List graph
         * central elements are adjusted with all element at lower level
         * side elements are adjusted with two near elements at lower level
         * 
         */

        private void CreateGraph(List<List<Pair<Vector2, Slot>>> list)
        {
            graph.Clear();
            for (int i = 4; i >= 0; i--)
            {
                for (int j = 2; j < 5; j++)
                {
                    graph.Add(new Node(list[i][j].Second));
                }
            }

            for (int i = 3; i < graph.Count; i++)
            {
                List<Node> nodes = new List<Node>();
                switch (i % 3)
                {
                    case (0):
                        nodes.Add(graph[i - 3]);
                        nodes.Add(graph[i - 2]);
                        graph[i].Nodes = nodes;
                        break;

                    case (1):
                        nodes.Add(graph[i - 4]);
                        nodes.Add(graph[i - 3]);
                        nodes.Add(graph[i - 2]);
                        graph[i].Nodes = nodes;
                        break;

                    case (2):
                        nodes.Add(graph[i - 4]);
                        nodes.Add(graph[i - 3]);
                        graph[i].Nodes = nodes;
                        break;
                }
            }
        }

        //Find all paths in graph and add them to pathLists
        //Total: 99 paths

        public List<Pair<SlotType, List<Slot>>> CheckWin(List<List<Pair<Vector2, Slot>>> list)
        {
            CreateGraph(list);

            List<Pair<SlotType, List<Slot>>> winList = new List<Pair<SlotType, List<Slot>>>();

            pathLists.Clear();
            for (int i = 12; i < 15; i++)
            {
                Depth(graph[i], new List<Node>());
            }

            Debug.Assert(pathLists.Count == 99);

            foreach(var path in pathLists)
            {
                //Check if all nodes has the same slot type
                SlotType type = path[0].Slot.Type;
                if (path.All(x => x.Slot.Type == type))
                {
                    Pair<SlotType, List<Slot>> localList = new Pair<SlotType, List<Slot>>(type, path.ConvertAll<Slot>(x => x.Slot));
                    if (winList.FirstOrDefault((x) => Equal(x, localList)) == null)
                    {
                        winList.Add(localList);
                    }
                    continue;
                }

                //Check if 4 nodes has the same slot type
                type = path[0].Slot.Type;
                List<Node> nodes = new List<Node>(path.Take(4));
                if (nodes.All(x => x.Slot.Type == type))
                {
                    Pair<SlotType, List<Slot>> localList = new Pair<SlotType, List<Slot>>(type, nodes.ConvertAll<Slot>(x => x.Slot));
                    if (winList.FirstOrDefault((x) => Equal(x, localList)) == null)
                    {
                        winList.Add(localList);
                    }
                    continue;
                }

                type = path[path.Count-1].Slot.Type;
                nodes = new List<Node>(path.Skip(1).Take(4));
                if (nodes.All(x => x.Slot.Type == type))
                {
                    Pair<SlotType, List<Slot>> localList = new Pair<SlotType, List<Slot>>(type, nodes.ConvertAll<Slot>(x => x.Slot));
                    if (winList.FirstOrDefault((x) => Equal(x, localList)) == null)
                    {
                        winList.Add(localList);
                    }
                    continue;
                }

                //Check if 3 nodes has the same slot type

                type = path[0].Slot.Type;
                nodes = new List<Node>(path.Take(3));
                if (nodes.All(x => x.Slot.Type == type))
                {
                    Pair<SlotType, List<Slot>> localList = new Pair<SlotType, List<Slot>>(type, nodes.ConvertAll<Slot>(x => x.Slot));
                    if (winList.FirstOrDefault((x) => Equal(x, localList)) == null)
                    {
                        winList.Add(localList);
                    }
                    continue;
                }

                type = path[1].Slot.Type;
                nodes = new List<Node>(path.Skip(1).Take(3));
                if (nodes.All(x => x.Slot.Type == type))
                {
                    Pair<SlotType, List<Slot>> localList = new Pair<SlotType, List<Slot>>(type, nodes.ConvertAll<Slot>(x => x.Slot));
                    if (winList.FirstOrDefault((x) => Equal(x, localList)) == null)
                    {
                        winList.Add(localList);
                    }
                    continue;
                }

                type = path[path.Count-1].Slot.Type;
                nodes = new List<Node>(path.Skip(2).Take(3));
                if (nodes.All(x => x.Slot.Type == type))
                {
                    Pair<SlotType, List<Slot>> localList = new Pair<SlotType, List<Slot>>(type, nodes.ConvertAll<Slot>(x => x.Slot));
                    if (winList.FirstOrDefault((x) => Equal(x, localList)) == null)
                    {
                        winList.Add(localList);
                    }
                    continue;
                }
            }

            return winList;
        }

        private bool Equal(Pair<SlotType, List<Slot>> first, Pair<SlotType, List<Slot>> second)
        {
            var firstNotSecond = first.Second.Except(second.Second).ToList();
            var secondNotFirst = second.Second.Except(first.Second).ToList();

            return !firstNotSecond.Any() && !secondNotFirst.Any() && first.First == second.First;
        }

        //Depth graph traversal
        private void Depth(Node node, List<Node> pathList)
        {
            List<Node> currentPathList = new List<Node>(pathList);
            currentPathList.Add(node);



            if (node.Nodes.Count > 0)
            {
                for (int i = 0; i < node.Nodes.Count; i++)
                {
                    Depth(node.Nodes[i], currentPathList);
                }
            }
            else
            {
                
                pathLists.Add(currentPathList);
            }
        }
    }
}
