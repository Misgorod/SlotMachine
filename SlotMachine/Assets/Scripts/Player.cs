﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Slot
{
    public class Player : IInitializable
    {
        public event EventHandler<UpdateDataEventArgs> OnUpdateData;

        private WinHandler winHandler;

        private decimal currentMoney = 1000;
        private decimal rollCost = 25;
        private decimal winAmount = 50;

        private bool isCostMultiplied = false;
        private decimal multiplier = 2m;

        public Player(WinHandler winHandler)
        {
            this.winHandler = winHandler;
        }

        public void Initialize()
        {
            UpdateViewData();
        }

        public bool CanRoll()
        {
            return currentMoney >= rollCost;
        }

        public void Roll()
        {
            currentMoney -= rollCost;
            UpdateViewData();
        }

        public void Multiply()
        {
            rollCost *= multiplier;
            winAmount *= multiplier;
            isCostMultiplied = !isCostMultiplied;
            if (!isCostMultiplied)
            {
                multiplier = 2m;
            }
            else
            {
                multiplier = 0.5m;
            }
            UpdateViewData();
        }

        private void UpdateViewData()
        {
            if (OnUpdateData != null)
            {
                OnUpdateData(this, new UpdateDataEventArgs(currentMoney, rollCost, winAmount));
            }
        }

        public void CheckWin(List<List<Pair<Vector2, Slot>>> slots)
        {
            var win = winHandler.CheckWin(slots);
            foreach (var list in win)
            {
                Debug.Log(list.First.ToString() + " " + list.Second.Count);
                currentMoney += winAmount;
            }
            UpdateViewData();
        }

        
    }

    public class UpdateDataEventArgs : EventArgs
    {
        public decimal Money { get; set; }
        public decimal RollCost { get; set; }
        public decimal WinAmount { get; set; }

        public UpdateDataEventArgs(decimal money, decimal rollCost, decimal winAmount)
        {
            Money = money;
            RollCost = rollCost;
            WinAmount = winAmount;
        }
    }
}
