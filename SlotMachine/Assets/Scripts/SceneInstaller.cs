﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zenject;

namespace Slot
{
    public class SceneInstaller : MonoInstaller<SceneInstaller>
    {
        public override void InstallBindings()
        {
            DeclareSignals();
            Container.BindInterfacesAndSelfTo<Player>().AsSingle();
            Container.Bind<WinHandler>().AsSingle();
        }

        private void DeclareSignals()
        {
            SignalBusInstaller.Install(Container);
            Container.DeclareSignal<RollStartsSignal>();
            Container.DeclareSignal<RollSignal>();
            Container.DeclareSignal<UpdatePlayerDataSignal>();
            Container.DeclareSignal<MultiplySignal>();
        }
    }
}
